import numpy as np
from datetime import datetime, timedelta


def stringify_date(value):
    # type: (datetime) -> str
    """
    Returns given datetime as string.

    :rtype: str
    :param value: datetime.
    :return: datetime string.
    """
    return datetime.strftime(value, "%Y-%m-%d_%H-%M-%S")


def to_unixtime(value):
    # type: (datetime) -> int
    """
    Returns given datetime as long unixtime.

    :rtype: int
    :param value: datetime.
    :return: unixtime.
    """
    if isinstance(value, np.datetime64):
        value = datetime.fromtimestamp(value.astype("O")/1e9)
    return int(datetime.timestamp(value) * 1e3)


def from_unixtime(value):
    # type: (int) -> datetime
    """
    Returns given long unixtime as datetime.

    :rtype: datetime
    :param value: unixtime.
    :return: datetime.
    """
    return datetime.fromtimestamp(value / 1e3)


def pretty_timedelta(td):
    # type: (timedelta) -> str
    """
    Converts given timedelta to pretty string.
    
    :rtype: str
    :param td: timedelta.
    :return: string representation.
    """
    seconds = abs(int(td.total_seconds()))
    seconds = abs(int(seconds))
    days, seconds = divmod(seconds, 86400)
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    if days > 0:
        return "%dd %dh" % (days, hours)
    elif hours > 0:
        return "%dd %dh" % (hours, minutes)
    elif minutes > 0:
        return "%dmin %dsec" % (minutes, seconds)
    else:
        return "%dsec" % seconds


if __name__ == "__main__":
    print(stringify_date(datetime.now()))
    dt = datetime.now()
    print(dt)
    unix_dt = to_unixtime(dt)
    print(unix_dt)
    dt = from_unixtime(unix_dt)
    print(dt)
    print(pretty_timedelta(timedelta(days=1, hours=2, minutes=3, seconds=4)))
