from time import sleep
from threading import Timer
from datetime import datetime


class DelayedCaller(object):
    def __init__(self, delays):
        # type: (dict) -> None
        """
        Initiates DelayedCaller object.

        :rtype: None
        :param delays: dictionary {tag: delay}.
        :return: None.
        """
        p = datetime(year=1970, month=1, day=1)
        self.__delays = delays
        self.__last_calls = {k: p for k in delays.keys()}

    def _call_delayed(self, tag, handler, *args, **kwargs):
        # type: (str, callable) -> object
        """
        Calls given handler taking delay for specified tag into account.

        :rtype: object
        :param tag: call tag.
        :param handler: a handler.
        :return: handler result.
        """

        def call_handler(_call):
            """
            Calls a handler and updates call results.
            """
            _call["result"] = handler(*args, **kwargs)
            _call["is_done"] = True

        diff = (datetime.now() - self.__last_calls[tag]).total_seconds()
        delay = 0.0 if diff > self.__delays[tag] else self.__delays[tag] - diff
        call = {"result": None, "is_done": False}
        t = Timer(delay, call_handler, args=[call])
        t.start()
        while not call["is_done"]:
            sleep(0.01)
        self.__last_calls[tag] = datetime.now()
        return call["result"]
