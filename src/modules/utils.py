import numpy as np


def get_exponent(value):
    # type: (float) -> int
    """
    Returns exponent of a given float.

    :rtype: int
    :param value: float value.
    :return: exponent number.
    """
    return int(np.floor(np.log10(np.abs(value))))
