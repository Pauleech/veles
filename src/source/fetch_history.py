import os
import config
from source.gram import Gram


if __name__ == "__main__":
    telegram = Gram(**config.telegram_params)
    telegram.fetch_message_history(config.telegram_source_channel,
                                   os.path.join(config.telegram_path, "history.pickle"))
