import re
import config


def strip_emoji(message):
    # type: (str) -> str
    """
    Returns message string without emoji.

    :rtype: list
    :param message: signal message.
    :return: stripped message.
    """
    compiled = re.compile("[\U00010000-\U0010ffff]", flags=re.UNICODE)
    return compiled.sub(r"", message)


def parse_message(message):
    # type: (str) -> dict
    """
    Parses a signal message to a dictionary.

    :rtype: dict
    :param message: signal message.
    :return: signal dictionary.
    """
    output = None
    txt = strip_emoji(message)
    rg = list()
    # region regex expression
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("((?:[a-zA-Z][a-zA-Z0-9]+))")  # Word 1
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(\\d+)")  # Integer Number 1
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(\\d+)")  # Integer Number 2
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(\\d+)")  # Integer Number 3
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(\\d+)")  # Integer Number 4
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(\\d+)")  # Integer Number 5
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(\\d+)")  # Integer Number 6
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(?:[a-zA-Z][a-zA-Z]+)")  # Uninteresting: word
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(?:[a-zA-Z][a-zA-Z]+)")  # Uninteresting: word
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("(?:[a-zA-Z][a-zA-Z]+)")  # Uninteresting: word
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("((?:[A-Z]+))")  # Word 2
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("([+-]?\\d*\\.\\d+)(?![-+0-9\\.])")  # Float 1
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("([+-]?\\d*\\.\\d+)(?![-+0-9\\.])")  # Float 2
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("([+-]?\\d*\\.\\d+)(?![-+0-9\\.])")  # Float 3
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("([+-]?\\d*\\.\\d+)(?![-+0-9\\.])")  # Float 4
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("([+-]?\\d*\\.\\d+)(?![-+0-9\\.])")  # Float 5
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("([+-]?\\d*\\.\\d+)(?![-+0-9\\.])")  # Float 6
    rg.append(".*?")  # Non-greedy match on filler
    rg.append("([+-]?\\d*\\.\\d+)(?![-+0-9\\.])")  # Float 7
    # endregion
    rg = re.compile("".join(rg), re.DOTALL)
    m = rg.search(txt)
    if m:
        output = {key: m.group(i+1) for i, key in enumerate(config.signal_keys)}
        cols = ["sell", "sellpc", "buy", "stop", "stoppc", "now", "nowpc"]
        output = {k: float(v) if k in cols else v for k, v in output.items()}
    return output


if __name__ == "__main__":
    print(parse_message(config.signal_test))
