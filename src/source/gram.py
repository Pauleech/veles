import config
import _pickle as pickle
from typing import Optional
from modules.timing import timeit
from telethon import TelegramClient
from modules.logering import setup_logger
from modules.delayed import DelayedCaller
logger = setup_logger(__name__, config.log_name, config.log_level)


class Gram(DelayedCaller):
    def __init__(self, telegram_api_id, telegram_api_hash, telegram_phone,
                 telegram_password=None, session="default"):
        # type: (int, str, str, str, str) -> None
        """
        Initiates Gram object.
        Class allows you to fetch messages from Telegram channels.

        :rtype: None
        :param telegram_api_id: Telegram API id.
        :param telegram_api_hash: Telegram API hash code.
        :param telegram_phone: Telegram phone number for login.
        :param telegram_password: Telegram password (2FA).
        :param session: session name.
        :return: None.
        """
        self.__tag = "t"
        DelayedCaller.__init__(self, {self.__tag: config.telegram_delay})
        # ----
        # telegram
        self.__client = None
        self.__telegram_api_id = telegram_api_id
        self.__telegram_api_hash = telegram_api_hash
        self.__telegram_phone = telegram_phone
        self.__telegram_password = telegram_password
        self.__session = session
        # ----
        # launch
        self.__login()

    def __login(self):
        # type: () -> None
        """
        Starts Telegram client.

        :rtype: None
        :return: None.
        """
        self.__client = TelegramClient(self.__session, self.__telegram_api_id,
                                       self.__telegram_api_hash, spawn_read_thread=False)
        self.__client.start(phone=self.__telegram_phone, password=self.__telegram_password)

    def get_messages(self, channel, min_id):
        # type: (int, int) -> list
        """
        Returns messages from a target channel.

        :rtype: list
        :param channel: target channel id.
        :param min_id: all the messages with a lower (older) ID or equal to this will be excluded.
        :return: messages list.
        """

        def get_message_dict(v):
            # type: (object) -> Optional[dict, None]
            """
            Returns message dictionary for a given MessageService.

            :rtype: dict
            :param v: MessageService object.
            :return: message dictionary.
            """
            try:
                return {"id": v.id, "date": v.date, "message": v.message}
            except Exception as e:
                logger.warn("..warn in get_messages() call: {}".format(e))
                return None

        output = self._call_delayed(self.__tag, self.__client.get_messages,
                                    channel, min_id=min_id, limit=None)  # type: list
        output = [get_message_dict(value) for value in output]
        output = [message for message in output if message is not None]
        return output

    def get_last_id(self, channel):
        # type: (int) -> int
        """
        Returns id of the last message.

        :rtype: int
        :param channel: target channel id.
        :return: id of the last message.
        """
        output = self._call_delayed(self.__tag, self.__client.get_messages, channel, limit=1)[0].id
        return output

    @timeit(logger, "fetching full message history", title=True)
    def fetch_message_history(self, channel, filename):
        # type: (int, str) -> None
        """
        Fetches and saves full message history from a target channel.

        :rtype: None
        :param channel: target channel id.
        :param filename: filename to save.
        :return: None.
        """
        messages = self.get_messages(channel, 0)
        with open(filename, "wb") as f:
            pickle.dump(messages, f)
