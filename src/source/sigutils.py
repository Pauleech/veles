def is_valid(signal, currencies):
    # type: (dict, list) -> bool
    """
    Returns True if signal is valid for trade.

    :rtype: bool
    :param signal: signal dictionary.
    :param currencies: list of market currencies.
    :return: True if signal is valid for trade.
    """
    if signal["sellpc"] > 30:
        return False
    if signal["stoppc"] > 30:
        return False
    if signal["sellpc"] < 0:
        return False
    if signal["stoppc"] < 0:
        return False
    if signal["coin"] not in currencies:
        return False
    return True
