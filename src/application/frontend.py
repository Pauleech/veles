import config
import strings
from queue import Queue
from modules.logering import setup_logger
from telegram.ext import Updater, CommandHandler
from application.core import BaseApplication, SenderApplication
from application.decorators import thread, redisop, botcommand, restricted
logger = setup_logger(__name__, config.log_name, config.log_level)


class Frontend(BaseApplication, SenderApplication):
    def __init__(self, redis_params, token):
        # type: (dict, str) -> None
        """
        Initiates Frontend object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :param token: telegram token.
        :return: None.
        """
        BaseApplication.__init__(self)
        SenderApplication.__init__(self, redis_params, "frontend")
        self.token = token
        # ----
        # redis
        self.posts_field = "posts"
        self.posts_queue = Queue()
        # ----
        # channel
        self.channels = {"posts": config.telegram_info_channel_id,
                         "logs": config.telegram_logs_channel_id}

    @redisop(logger)
    def __pull_redis_post(self):
        # type: () -> None
        """
        Pulls post from redis and puts to the appropriate posts queue.

        :rtype: None
        :return: None.
        """
        pulled = self._redis.rpop(self.posts_field)
        if pulled is not None:
            post = self._unpack_data(pulled)  # type: dict
            self.posts_queue.put(post)

    @thread(logger)
    def __listen_posts(self, bot):
        # type: () -> None
        """
        Listens for the new posts in redis.

        :rtype: None
        :return: None.
        """
        try:
            self.__pull_redis_post()
        except Exception as e:
            logger.error("..error in listen_posts thread: {}".format(e))
            bot.sendMessage(chat_id=self.channels["logs"], text=config.telegram_posts["error"].format(e))

    @thread(logger)
    def __publish_posts(self, bot):
        # type: (object) -> None
        """
        Publishes posts into a channel.

        :rtype: None
        :param bot: telegram bot.
        :return: None.
        """
        try:
            post = self.posts_queue.get()
            args = config.telegram_posts_keys[post["type"]]
            args = [post[arg] for arg in args]
            text = config.telegram_posts[post["type"]].format(*args)
            bot.sendMessage(chat_id=self.channels[post["channel"]], text=text)
        except Exception as e:
            logger.error("..error in publish_posts thread: {}".format(e))
            bot.sendMessage(chat_id=self.channels["logs"], text=config.telegram_posts["error"].format(e))

    @botcommand(logger)
    @restricted(logger)
    def command_start(self, bot, update):
        # type: (object, object) -> None
        """
        Specifies bot /start command actions.

        :rtype: None
        :param bot: telegram bot.
        :param update: telegram update.
        :return: None.
        """
        self.stop_threads()
        self.posts_queue = Queue()
        self._add_task(self.__listen_posts, config.time_eps, bot)
        self._add_task(self.__publish_posts, config.time_eps, bot)
        self.run_polling()
        update.message.reply_text(strings.telegram_greeting_post)
        bot.sendMessage(chat_id=self.channels["logs"], text=strings.telegram_restart_post)

    @botcommand(logger)
    @restricted(logger)
    def command_info(self, bot, update):
        # type: (object, object) -> None
        """
        Specifies bot /info command actions.

        :rtype: None
        :param bot: telegram bot.
        :param update: telegram update.
        :return: None.
        """
        request = self._new_request("get_info")
        index = self._push_redis_request(request, "backend")
        post = self._pull_redis_answer(-index, "backend")  # wait
        if post is not None:
            args = config.telegram_posts_keys[post["type"]]
            args = [post[arg] for arg in args]
            text = config.telegram_posts[post["type"]].format(*args)
            update.message.reply_text(text)
        else:
            update.message.reply_text(strings.telegram_no_answer_post)

    def bot_error(self, bot, update, error):
        # type: (object, object, object) -> None
        """
        Specifies bot error actions.

        :rtype None
        :param bot: telegram bot.
        :param update: telegram update.
        :param error: telegram error.
        :return: None.
        """
        if update is not None:
            logger.error("update {} caused error {}".format(update, error))
            bot.sendMessage(chat_id=self.channels["logs"], text=config.telegram_posts["error"].format(error))

    def run_bot(self):
        # type: () -> None
        """
        Starts telegram bot.

        :rtype None
        :return: None.
        """
        updater = Updater(self.token)
        dp = updater.dispatcher
        dp.add_handler(CommandHandler("start", self.command_start))
        dp.add_handler(CommandHandler("info", self.command_info))
        dp.add_error_handler(self.bot_error)
        updater.start_polling(timeout=20, poll_interval=1.0)
        updater.idle()
