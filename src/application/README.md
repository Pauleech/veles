## Overview

Folder contains application related modules together with basic classes and decorators for creating a multi-threaded application with a shared Redis space.
 
 
## Thredis 

### Glossary

* [Redis](https://redis.io) – an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs and geospatial indexes with radius queries. Redis has built-in replication, Lua scripting, LRU eviction, transactions and different levels of on-disk persistence, and provides high availability via Redis Sentinel and automatic partitioning with Redis Cluster. 

### Multi-Threaded Application

`/src/application/core.py` provides `BaseApplication` class which inherited allows to make multithreaded applications:

```
app._add_task(handler, delay)  # adds a function to threading tasks
app.run_polling()  # runs the application polling starting all the threads
```

`@thread` decorator from `/src/application/decorators.py` has to be used on the functions marking them as threads.

### Redis Application

`/src/application/core.py` provides also `SenderApplication` and `ReceiverApplication` classes which are used in order to add applications the ability to communicate among themselves through requests:

<p align="center">
  <img src="/uploads/2a08db2e131e1257ec6fcb8681a5e89f/thredis_scheme.png" width="600px" >
</p>


#### Usage

* `SenderApplication` object side:

```
request = self._new_request(some_function.__name__, args=args, kwargs=kwargs)
index = self._push_redis_request(request, "backend")
answer = self.pull_redis_answer(-index, "backend")  # wait
```

* `ReceiverApplication` object side has to contain `some_function`.
