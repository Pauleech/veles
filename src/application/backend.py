import os
import config
import pandas as pd
import _pickle as pickle
from queue import Queue
from source.gram import Gram
from source.sigutils import is_valid
from source.messages import parse_message
from modules.logering import setup_logger
from exchange.bittrex import BittrexExchange
from application.core import ReceiverApplication
from application.decorators import thread, redisop, requestable
logger = setup_logger(__name__, config.log_name, config.log_level)


class Backend(ReceiverApplication):
    def __init__(self, redis_params):
        # type: (dict) -> None
        """
        Initiates Backend object.

        :rtype: None
        :param redis_params: redis shared space parameters dictionary.
        :return: None.
        """
        ReceiverApplication.__init__(self, redis_params, "backend")
        # ----
        # functionality
        self.__telegram = Gram(**config.telegram_params)
        self.__bittrex = BittrexExchange(quote="BTC", path=config.ohlcv_path)
        self.__telegram_last_id = self.__telegram.get_last_id(config.telegram_source_channel)
        self.__signals_queue = Queue()
        # ----
        # portfolio
        self.__current_trades = None
        self.__load_trades(os.path.join(config.data_path, "trades.pickle"))
        self.__balance_total = self.__bittrex.get_total_balance()
        self.__balance_splits = config.bittrex_balance_splits
        # ----
        # redis
        self.posts_field = "posts"
        # ----
        # history
        self.__history = None
        self.__load_history(os.path.join(config.data_path, "history.csv"))
        # ----
        # threading
        self._add_task(self.__listen_messages, config.time_eps)
        self._add_task(self.__process_signals, config.time_eps)
        self._add_task(self.__check_trades, 30)
        self._add_task(self.__update_balance, 300)

    def __save_trades(self, filename):
        # type: (str) -> None
        """
        Saves current trades into .pickle file.

        :rtype: None
        :param filename: filename to save.
        :return: None.
        """
        with open(filename, "wb") as f:
            pickle.dump(self.__current_trades, f)

    def __load_trades(self, filename):
        # type: (str) -> None
        """
        Loads current trades from .pickle file.

        :rtype: None
        :param filename: filename to load.
        :return: None.
        """
        try:
            with open(filename, "rb") as f:
                self.__current_trades = pickle.load(f)
            logger.info("..current trades:")
            for trade in self.__current_trades:
                logger.info(trade)
        except IOError:
            self.__current_trades = []
            logger.info("..there are no current trades")

    def __add_to_history(self, trade):
        # type: (dict) -> None
        """
        Adds trade to history.

        :rtype: None
        :param trade: trade dictionary.
        :return: None.
        """
        self.__history.append(trade)
        self.__save_history(os.path.join(config.data_path, "history.csv"))

    def __save_history(self, filename):
        # type: (str) -> None
        """
        Saves trades history into .csv file.

        :rtype: None
        :param filename: filename to save.
        :return: None.
        """
        cols = ["datetime", "pair", "type", "price", "amount", "total"]
        data = pd.DataFrame(self.__history, columns=cols)
        data.to_csv(filename, float_format="%.8f", index=False)

    def __load_history(self, filename):
        # type: (str) -> None
        """
        Loads trades history from .csv file.

        :rtype: None
        :param filename: filename to load.
        :return: None.
        """
        try:
            self.__history = pd.read_csv(filename)  # type: pd.DataFrame
            self.__history["datetime"] = pd.to_datetime(self.__history["datetime"])
            self.__history = self.__history.to_dict("records")
            self.__history = [{k: v.to_pydatetime() if k == "datetime" else v for k, v in d.items()}
                              for d in self.__history]
        except IOError:
            self.__history = list()
            logger.info("..there is no trades history")

    def __parse_messages(self, messages):
        # type: (list) -> None
        """
        Parses signal messages and adds them to signals queue.

        :rtype: None
        :param messages: list of signals messages.
        :return: None.
        """
        for i, message in enumerate(messages):
            if message["message"] is None:
                continue
            if "Original signal quote" not in message["message"]:
                continue
            signal = parse_message(message["message"])
            if signal is not None:
                signal["datetime"] = message["date"]
                signal["id"] = message["id"]
                signal["message"] = message["message"]
                self.__signals_queue.put(signal)

    @redisop(logger)
    def __push_redis_post(self, post):
        # type: (dict) -> None
        """
        Pushes post to the redis posts queue.

        :rtype: None
        :param post: post dictionary.
        :return: None.
        """
        self._redis.lpush(self.posts_field, self._pack_data(post))

    @requestable(logger)
    def get_info(self):
        # type: () -> dict
        """
        Returns current trades and balance state dictionary.

        :rtype: dict
        :return: current trades and balance state dictionary.
        """
        try:
            output = {"number": len(self.__current_trades), "balance_total": self.__balance_total,
                      "channel": "posts", "type": "info"}
            return output
        except Exception as e:
            logger.error(e)

    @thread(logger)
    def __listen_messages(self):
        # type: () -> None
        """
        Listens for the new signal messages.

        :rtype: None
        :return: None.
        """
        try:
            messages = self.__telegram.get_messages(config.telegram_source_channel, self.__telegram_last_id)
            if len(messages) != 0:
                self.__telegram_last_id = messages[0]["id"]
                self.__parse_messages(messages)
        except Exception as e:
            logger.error("..error in listen_messages thread: {}".format(e))
            self.__push_redis_post({"channel": "logs", "type": "error", "text": e})

    @thread(logger)
    def __process_signals(self):
        # type: () -> None
        """
        Processes signals in queue.

        :rtype: None
        :return: None.
        """
        try:
            signal = self.__signals_queue.get()
            if is_valid(signal, self.__bittrex.currencies):
                pair, sell, stop = signal["coin"] + "/BTC", signal["sell"], signal["stop"]
                output = self.__bittrex.perform_buy(pair, self.__balance_total, self.__balance_splits, sell, stop)
                if output["success"]:
                    logger.info("..bought {:.8f} of {} for {:.8f}.".format(output["amount"], pair, output["price"]))
                    self.__current_trades.append(output)
                    self.__push_redis_post({**{"channel": "posts"}, **output})
                    self.__save_trades(os.path.join(config.data_path, "trades.pickle"))
                    self.__add_to_history(output)
                else:
                    self.__push_redis_post({**{"channel": "logs"}, **output})
                    if output["type"] == "buy_price_jumps":
                        self.__signals_queue.put(signal)  # new try to buy a coin
            else:
                logger.warn("..cannot buy {}: signal is invalid".format(signal["coin"]))
                self.__push_redis_post({"channel": "logs", "type": "invalid_signal", "text": signal["message"]})
        except Exception as e:
            logger.error("..error in process_feeds thread: {}".format(e))
            self.__push_redis_post({"channel": "logs", "type": "error", "text": e})

    @thread(logger)
    def __check_trades(self):
        # type: () -> None
        """
        Checks current trades for sale.

        :rtype: None
        :return: None.
        """
        try:
            if len(self.__current_trades) != 0:
                tickers = self.__bittrex.get_tickers()
                for trade in list(self.__current_trades):
                    if tickers[trade["pair"]]["bid"] > trade["sell"]:
                        output = self.__bittrex.perform_sell(trade["pair"], trade["amount"], trade["sell"])
                    elif tickers[trade["pair"]]["bid"] < trade["stop"]:
                        output = self.__bittrex.perform_stop_sell(trade["pair"], trade["amount"], trade["stop"])
                    else:
                        continue
                    if output["success"]:
                        output["outcome"] = (output["price"] - trade["price"]) * trade["amount"]
                        output["balance_total"] = self.__balance_total
                        logger.info("..sold {:.8f} of {}, outcome: {:.8f}."
                                    .format(output["amount"], output["pair"], output["outcome"]))
                        self.__current_trades.remove(trade)
                        self.__push_redis_post({**{"channel": "posts"}, **output})
                        self.__save_trades(os.path.join(config.data_path, "trades.pickle"))
                        self.__add_to_history(output)
                    else:
                        self.__push_redis_post({**{"channel": "logs"}, **output})
                        if output["type"] != "sell_price_jumps":
                            self.__current_trades.remove(trade)
                            self.__save_trades(os.path.join(config.data_path, "trades.pickle"))
        except Exception as e:
            logger.error("..error in check_trades thread: {}".format(e))
            self.__push_redis_post({"channel": "logs", "type": "error", "text": e})

    @thread(logger)
    def __update_balance(self):
        # type: () -> None
        """
        Updates total balance.

        :rtype: None
        :return: None.
        """
        try:
            self.__balance_total = self.__bittrex.get_total_balance()
        except Exception as e:
            logger.error("..error in listen_messages thread: {}".format(e))
            self.__push_redis_post({"channel": "logs", "type": "error", "text": e})


if __name__ == "__main__":
    backend = Backend(config.redis_params)
    backend.run_polling()
