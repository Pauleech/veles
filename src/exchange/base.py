import os
import config
import pandas as pd


class BaseExchange(object):
    def __init__(self, quote, timeframe, path=None):
        # type: (str, str, str) -> None
        """
        Initiates BaseExchange object.

        :rtype: None
        :param quote: quote currency.
        :param timeframe: OHLCV timeframe.
        :param path: load OHLCV from path if possible.
        :return: None.
        """
        self._quote = quote
        self._timeframe = timeframe
        self.__path = path
        self.pairs = []

    def _get_filename(self, pair):
        # type: (str) -> str
        """
        Returns filename for a given market currency pair.

        :rtype: str
        :param pair: market symbol.
        :return: filename.
        """
        return os.path.join(self.__path, pair.replace("/", "") + ".csv")

    @staticmethod
    def _save_ohlcv(data, filename):
        # type: (DataFrame, str) -> None
        """
        Saves OHLCV DataFrame.

        :rtype: None
        :param data: OHLCV DataFrame.
        :param filename: filename to save.
        :return: None.
        """
        data.to_csv(filename, float_format="%.8f", index=False)

    @staticmethod
    def _load_ohlcv(filename):
        # type: (str) -> DataFrame
        """
        Loads OHLCV DataFrame.

        :rtype: DataFrame
        :param filename: filename to load.
        :return: OHLCV DataFrame.
        """
        data = pd.read_csv(filename)
        data["Timestamp"] = pd.to_datetime(data["Timestamp"])
        return data

    @staticmethod
    def _process_ohlcv(data):
        # type: (list) -> DataFrame
        """
        Returns preprocessed OHLCV DataFrame for given list of lists.

        :rtype: DataFrame
        :param data: OHLCV list of lists
        :return: OHLCV DataFrame.
        """
        output = pd.DataFrame(data, columns=["Timestamp"] + config.price_columns + ["Volume"])
        output["Timestamp"] = pd.to_datetime(output["Timestamp"], unit="ms", utc=True)
        output["Volume"].replace(0, 1, inplace=True)
        return output
