import ccxt
import config
import pandas as pd
import exchange.tradeutils as trades
from modules.timing import timeit
from exchange.base import BaseExchange
from modules.timing import elapsed_timer
from datetime import datetime, timedelta
from modules.delayed import DelayedCaller
from modules.logering import setup_logger
from modules.dateutils import to_unixtime, from_unixtime
logger = setup_logger(__name__, config.log_name, config.log_level)


class BittrexExchange(BaseExchange, DelayedCaller):
    def __init__(self, quote="BTC", timeframe="1m", path=None):
        # type: (str, str, str) -> None
        """
        Initiates BinanceExchange object.

        :rtype: None
        :param quote: quote currency.
        :param timeframe: OHLCV timeframe.
        :param path: load OHLCV from path if possible.
        :return: None.
        """
        BaseExchange.__init__(self, quote, timeframe, path)
        # ----
        # exchange
        self.__marketplace = ccxt.bittrex({
            "apiKey": config.bittrex_key,
            "secret": config.bittrex_secret
        })
        self.__marketplace.timeout *= 2
        # ----
        # api delay
        self.__tag = "b"
        DelayedCaller.__init__(self, {self.__tag: (self.__marketplace.rateLimit / 1000) * 1.5})
        # ----
        self.pairs = self.__get_currency_pairs()
        self.currencies = [pair[:-4] for pair in self.pairs]

    def __get_currency_pairs(self):
        # type: () -> list
        """
        Returns list of market currency pairs.

        :rtype: list
        :return: list of market currency pairs.
        """
        markets = self._call_delayed(self.__tag, self.__marketplace.fetch_markets)  # type: list
        pairs = [m["symbol"] for m in markets if m["quote"] == self._quote]
        return pairs

    def __fetch_ohlcv(self, pair):
        # type: (str) -> DataFrame
        """
        Fetches OHLCV DataFrame for a given market currency pair.

        :rtype: DataFrame
        :param pair: market currency pair.
        :return: OHLCV DataFrame.
        """
        output = []
        since = to_unixtime(datetime(2017, 10, 1))
        last_date = to_unixtime(datetime.today() - timedelta(hours=1))
        with elapsed_timer(logger, "fetching ohlcv {}".format(pair), title=True):
            while since < last_date:
                data = self._call_delayed(self.__tag, self.__marketplace.fetch_ohlcv, pair,
                                          timeframe=self._timeframe, since=since)  # type: list
                data = self._process_ohlcv(data)  # type: pd.DataFrame
                if len(data) == 0:
                    break
                since = to_unixtime(data["Timestamp"].values[-1])
                logger.info("..{}".format(from_unixtime(since)))
                since += 1
                output.append(data)
        output = pd.concat(output).reset_index(drop=True)
        return output

    def get_ohlcv(self, pair):
        # type: (str) -> DataFrame
        """
        Returns OHLCV DataFrame for a given market currency pair.

        :rtype: DataFrame
        :param pair: market currency pair.
        :return: OHLCV DataFrame.
        """
        try:
            filename = self._get_filename(pair)
            output = self._load_ohlcv(filename)
        except FileNotFoundError:
            try:
                output = self.__fetch_ohlcv(pair)
                self._save_ohlcv(output, self._get_filename(pair))
                filename = self._get_filename(pair)
                output = self._load_ohlcv(filename)
            except Exception as e:
                logger.error(e)
                output = None
        return output

    @timeit(logger, "fetching full message history", title=True)
    def fetch_ohlcv_history(self):
        # type: () -> dict
        """
        Returns dictionary with OHLCV for a all market currency pairs.

        :rtype: dict
        :return: dictionary with OHLCV for a all market currency pairs.
        """
        output = dict()
        for symbol in self.pairs:
            output[symbol] = self.get_ohlcv(symbol)
        return output

    def get_tickers(self):
        # type: () -> dict
        """
        Returns current tickers dictionary.

        :rtype: dict
        :return: current tickers dictionary.
        """
        output = self._call_delayed(self.__tag, self.__marketplace.fetch_tickers)  # type: dict
        return output

    def get_total_balance(self):
        # type: () -> float
        """
        Returns current total balance in quote.

        :rtype: float
        :return: current total balance in quote.
        """
        tickers = self._call_delayed(self.__tag, self.__marketplace.fetch_tickers)  # type: dict
        amounts = self._call_delayed(self.__tag, self.__marketplace.fetch_total_balance)  # type: dict
        output = amounts[self._quote]
        amounts = {k + "/" + self._quote: v for k, v in amounts.items() if k != self._quote}
        output += sum([v * tickers[k]["last"] for k, v in amounts.items() if k in tickers])
        return output

    def __get_buy_amount_price(self, pair, price_total):
        # type: (str, float) -> (float, float)
        """
        Return amount and buy price for a given currency pair and total price.

        :rtype: (float, float)
        :param pair: market currency pair.
        :param price_total: total trade price.
        :return: amount and buy price.
        """
        book = self._call_delayed(self.__tag, self.__marketplace.fetch_order_book, pair)["asks"]
        price, filled = book[0]
        amount = price_total / price
        for i in range(1, len(book)):
            if filled <= amount:
                price, filled = book[i][0], filled + book[i][1]
            else:
                break
        return amount, price

    def __get_sell_price(self, pair, amount):
        # type: (str, int) -> float
        """
        Return sell price for a given currency pair and amount.

        :rtype: float
        :param pair: market currency pair.
        :param amount: amount to sell.
        :return: sell price.
        """
        book = self._call_delayed(self.__tag, self.__marketplace.fetch_order_book, pair)["bids"]
        price, filled = book[0]
        for i in range(1, len(book)):
            if filled <= amount:
                price, filled = book[i][0], filled + book[i][1]
            else:
                break
        return price

    def perform_buy(self, pair, balance_total, balance_splits, sell_price, stop_price):
        # type: (str, float, int, float, float) -> dict
        """
        Tries to buy a currency and returns a dictionary with purchase information.

        :rtype: dict
        :param pair: market currency pair.
        :param balance_total: estimated total portfolio balance.
        :param balance_splits: number of diversification splits.
        :param sell_price: sell price.
        :param stop_price: stop-loss price
        :return: dictionary with purchase information.
        """
        output = {"success": False, "pair": pair, "sell": sell_price, "stop": stop_price}
        try:
            balance_free = self._call_delayed(self.__tag, self.__marketplace.fetch_free_balance)[self._quote]
            price_total = balance_total / balance_splits
            is_ok, output = trades.is_enough_balance(output, balance_free, price_total)
            if not is_ok:
                return output
            amount, price = self.__get_buy_amount_price(pair, price_total)
            is_ok, output = trades.is_not_risky(output, price)
            if not is_ok:
                return output
            uuid = self._call_delayed(self.__tag, self.__marketplace.create_order,
                                      pair, "limit", "buy", amount, price)["id"]
            is_open = self._call_delayed(self.__tag, self.__marketplace.fetch_order, uuid, pair)["info"]["IsOpen"]
            if is_open:
                self._call_delayed(self.__tag, self.__marketplace.cancel_order, uuid, pair)
                logger.warn("..cannot buy {}: price jumps".format(pair))
                return {**{"type": "buy_price_jumps"}, **output}
            output["success"] = True
            return {**{"type": "buy", "amount": amount, "price": price,
                       "total": amount * price, "datetime": datetime.utcnow(), "uuid": uuid}, **output}
        except Exception as e:
            logger.warn("..cannot buy {}: {}".format(output["pair"], e))
            return {**{"type": "error", "text": e}, **output}

    def perform_sell(self, pair, amount, sell_price):
        # type: (str, int, float) -> dict
        """
        Tries to sell a currency and returns a dictionary with trade information.

        :rtype: dict
        :param pair: market currency pair.
        :param amount: amount to sell.
        :param sell_price: sell price.
        :return: dictionary with trade information.
        """
        output = {"success": False, "pair": pair, "sell": sell_price}
        try:
            price = self.__get_sell_price(pair, amount)
            if price < sell_price:
                logger.warn("..cannot sell {}: price = {:.8f}, sell = {:.8f}".format(pair, price, sell_price))
                return {**{"type": "sell_price_jumps"}, **output}
            uuid = self._call_delayed(self.__tag, self.__marketplace.create_order,
                                      pair, "limit", "sell", amount, price)["id"]
            is_open = self._call_delayed(self.__tag, self.__marketplace.fetch_order, uuid, pair)["info"]["IsOpen"]
            if is_open:
                self._call_delayed(self.__tag, self.__marketplace.cancel_order, uuid, pair)
                logger.warn("..cannot sell {}: price jumps".format(pair))
                return {**{"type": "sell_price_jumps"}, **output}
            output["success"] = True
            return {**{"type": "sell", "amount": amount, "price": price,
                       "total": amount * price, "datetime": datetime.utcnow(), "uuid": uuid}, **output}
        except Exception as e:
            logger.warn("..cannot sell {}: {}".format(output["pair"], e))
            return {**{"type": "error", "text": e}, **output}

    def perform_stop_sell(self, pair, amount, stop_price):
        # type: (str, int, float) -> dict
        """
        Tries to stop-sell a currency and returns a dictionary with trade information.

        :rtype: dict
        :param pair: market currency pair.
        :param amount: amount to sell.
        :param stop_price: sell price.
        :return: dictionary with trade information.
        """
        output = {"success": False, "pair": pair, "stop": stop_price}
        try:
            price = self.__get_sell_price(pair, amount)
            if price > stop_price:
                logger.warn("..cannot stop-sell {}: price = {:.8f}, stop = {:.8f}".format(pair, price, stop_price))
                return {**{"type": "sell_price_jumps"}, **output}
            uuid = self._call_delayed(self.__tag, self.__marketplace.create_order,
                                      pair, "limit", "sell", amount, price)["id"]
            is_open = self._call_delayed(self.__tag, self.__marketplace.fetch_order, uuid, pair)["info"]["IsOpen"]
            if is_open:
                self._call_delayed(self.__tag, self.__marketplace.cancel_order, uuid, pair)
                logger.warn("..cannot stop-sell {}: price jumps".format(pair))
                return {**{"type": "sell_price_jumps"}, **output}
            output["success"] = True
            return {**{"type": "stop", "amount": amount, "price": price,
                       "total": amount * price, "datetime": datetime.utcnow(), "uuid": uuid}, **output}
        except Exception as e:
            logger.warn("..cannot stop-sell {}: {}".format(output["pair"], e))
            return {**{"type": "error", "text": e}, **output}


if __name__ == "__main__":
    exchange = BittrexExchange(quote="BTC", timeframe="1h", path=config.ohlcv_path)
    print(exchange.get_total_balance())
