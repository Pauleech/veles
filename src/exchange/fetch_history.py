import config
from exchange.bittrex import BittrexExchange


if __name__ == "__main__":
    exchange = BittrexExchange(quote="BTC", timeframe="1h", path=config.ohlcv_path)
    exchange.fetch_ohlcv_history()
