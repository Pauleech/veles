import config
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def is_enough_balance(output, balance_free, price_total):
    # type: (dict, float, float) -> (bool, dict)
    """
    Check whether balance is enough to perform a trade.

    :rtype: (bool, dict)
    :param output: the result dictionary of the buy operation.
    :param balance_free: available balance.
    :param price_total: the cost of trade.
    :return: True/False and output dictionary.
    """
    if balance_free < price_total:
        logger.warn("..cannot buy {}: not enough balance, {:.8f} BTC".format(output["pair"], balance_free))
        return False, {**{"type": "not_enough_balance", "balance": balance_free}, **output}
    else:
        return True, output


def is_not_risky(output, price):
    # type: (dict, float) -> (bool, dict)
    """
    Check whether balance is enough to perform a trade.

    :rtype: (bool, dict)
    :param output: the result dictionary of the buy operation.
    :param price: trade price.
    :return: True/False and output dictionary.
    """
    if price * 1.02 > output["sell"] or price < output["stop"] * 1.02:
        logger.warn("..cannot buy {}: price = {:.8f}, sell = {:.8f}, stop = {:.8f}"
                    .format(output["pair"], price, output["sell"], output["stop"]))
        return False, {**{"type": "buy_high_risk", "price": price}, **output}
    else:
        return True, output
