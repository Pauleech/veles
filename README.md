# @veles

### Bittrex Trading Bot Based on Telegram Parsed Signals

## Tags

`cryptocurrency`, `trading`, `python`, `telethon`, `telegram api`, `bot interface`, `ccxt`

## Introduction

The main objective of this project is to develop an automated system that allows to purchase and sale cryptocurrencies based on continuously incoming signals. The source of the signals is the Telegram channel where the signals for cryptotrading are aggregated from more than 100 different channels. During the period of the cryptocurrency boom (winter 2017-2018), a dozen signals a day came from the selected channel. Channel crawling is based on the Telethon framework. The project implements parsing of received signals and their further processing. The continuous operation of the system is based on the multi-threaded architecture with a shared Redis space. Based on the received and processed signals, the backend makes transactions on the [Bittrex](https://www.bittrex.com) crypto exchange. The interface of the system, in turn, is represented in the form of a conventional Telegram bot, which notifies about purchases and sales, stop-loss sales and the current balance.

## Getting Started

These instructions allow you to reproduce the project and run it on your local machine for development and testing purposes.

### Prerequisites

The following software was used in this project:

* PyCharm: Python IDE for Professional Developers by JetBrains;
* Anaconda 4.4.10 Python 3.6 version;
* Redis 4.0.5 version;
* Python modules can be installed by `pip install`.

### Project structure

    ├── data                                    # data files
        ├── ohclv                               # ohclv files
        ├── telegram                            # telegram channel messages history
        ├── ...
    ├── src                                     # project source
        ├── application                         # basic classes and decorators together with application implementation
        ├── exchange                            # crypto exchange related modules
        ├── modules                             # additional modules of a common purpose
        ├── source                              # telethon (source) related modules
    ├── config.py
    ├── main.py
    ├── ...

`/src` has to be marked in PyCharm as sources root folder.

The project expects `credentials.py` file with Bittrex API and Telegram API tokens. This file is omitted in the repository due to security issues.

Folder `/src/application` contains its own README.md file.

### Glossary

* [Bittrex](https://www.bittrex.com) – a US-based cryptocurrency exchange headquartered in Seattle, Washington. The exchange the thirteenth largest cryptocurrency exchange by daily trading volume, is renowned for the vast number of cryptocurrencies it has listed, and has a good reputation for its security. Bittrex is not a regulated exchange under US securities laws.

* [Telegam](https://telegram.org) – a non-profit cloud-based instant messaging service. Telegram client apps exist for Android, iOS, Windows Phone, Windows NT, macOS and Linux. Users can send messages and exchange photos, videos, stickers, audio and files of any type. In February 2016, Telegram stated that it had 100 million monthly active users, sending 15 billion messages per day. According to its CEO, as of April 2017, Telegram has more than 50% annual growth rate. Telegram proposes a platform for third-party developers to create bots. Bots are Telegram accounts operated by programs. They can respond to messages, can be invited into groups and can be integrated into other programs.

### Data

The bot stores the history of operations in a separate file, in addition, current transactions are also stored in a separate file to be able to recover them when the bot reboots.
These files are supposed to be kept in `/data` folder.

## Running the project

### Getting Telethon access

In order for the bot to receive fresh signals from the Telegram channel using Telethon, it is necessary to create a Telegram account and set up a double authentication. At the first start it will be necessary to confirm the entry with the code from the SMS.

Before working with Telegram API, you need to get your own API ID and hash:

* Follow this [link](https://my.telegram.org/auth) and login with your phone number;
* Click under API Development tools;
* `Create new application` window will appear. Fill in your application details. There is no need to enter any URL, and only the first two fields (App title and Short name) can currently be changed later;
* Click on `Create application` at the end. Remember that your API hash is secret and Telegram won’t let you revoke it. Don’t post it anywhere!

Afterwards it is necessary to fill in the following fields in `credentials.py` located in root folder:

```
telegram_api_id = 123456
telegram_api_hash = "#####"
telegram_phone = "+123456789"
````

### Getting Telegram token

The Bot API is an HTTP-based interface created for developers keen on building bots for Telegram. Each bot is given a unique [authentication token](https://core.telegram.org/bots/api) when it is created.

Visit this [link](https://core.telegram.org/bots) in order to create a Telegram bot.

Afterwards it is necessary to fill in the following field in `credentials.py` located in root folder:

```
telegram_token = "#####"
````

You should also add your own Telegram ID to `config.py`:

```
telegram_allowed_users = [#####]
```

### Preparing Telegram channels

It is necessary to create two channels in the Telegram and give the bot administrator rights. One channel will be used for transaction information, and the second for logs.

Afterwards it is necessary to fill in the following fields in `credentials.py` located in root folder:

```
telegram_info_channel_id = #####
telegram_logs_channel_id = #####
````

### Getting Bittrex access

In order to be able to make transactions on the Bittrex exchange, you should register an account on the exchange, pass validation and create an API key.

Afterwards it is necessary to fill in the following field in `credentials.py` located in root folder:

```
bittrex_key = "#####"
bittrex_secret = "#####"
```

### Signals source

The source of the signals is the Telegram [channel](https://telegra.ph/Telegram-Ranked-Aggregated-Signals-10-23) called `Top Crypto Signals`, where signals from many popular cryptocurrency channels are aggregated in a form convenient for parsing. Access to this channel is absolutely free for anyone.

<p align="center">
  <img src="/uploads/cfd30652b4be220479b49f038a194417/channel.png" width="600px" >
</p>

### Running the application

The main file is `main.py` located in the root folder.

<p align="center">
  <img src="/uploads/34ed99a9c35ee7f26fd05f9f0095aecd/bot_start.png" width="600px" >
</p>

The bot automatically starts checking the channel for new signals and making transactions as soon as the signals are received.

<p align="center">
  <img src="/uploads/f6d006bfab7a1600e0410433619e7d68/bot_screenshot_01.png" width="600px" >
</p>

False signals are filtered, including too risky or if the price of the asset is too unstable. The bot also exits the trade using stop-loss, since this value is indicated in the signal message.

<p align="center">
  <img src="/uploads/3ce324dd75b2a03fad0c271f5308cdea/bot_screenshot_02.png" width="600px" >
</p>
