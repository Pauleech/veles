import config
from application.backend import Backend
from application.frontend import Frontend


def main():
    """
    Main script
    """
    back = Backend(config.redis_params)
    back.run_polling()
    front = Frontend(config.redis_params, config.telegram_token)
    front.run_bot()


if __name__ == "__main__":
    main()
