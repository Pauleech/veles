# telegram channel texts
telegram_buy_post = ("📝We have a new purchase!\n"
                     "\n"
                     "💎Coin: {}\n"
                     "🗿Amount: {:.8f}.\n"
                     "💰Price: {:.8f}.\n"
                     "💰Spent: {:.8f}.\n"
                     "\n"
                     "📈To sell at: {:.8f}.\n"
                     "⚠️Stop-loss: {:.8f}.")
telegram_sell_post = ("✅We have sold a coin!\n"
                      "\n"
                      "💎Coin: {}\n"
                      "🗿Amount: {:.8f}.\n"
                      "📈Price: {:.8f}.\n"
                      "💰Received: {:.8f}.\n"
                      "\n"
                      "⬆️Profit: {:.8f}.\n"
                      "💰Balance: {:.8f}.")
telegram_stop_post = ("🔴We sold under the stop-loss..\n"
                      "\n"
                      "💎Coin: {}\n"
                      "🗿Amount: {:.8f}.\n"
                      "📉Price: {:.8f}.\n"
                      "💰Received: {:.8f}.\n"
                      "\n"
                      "⬇️Loss: {:.8f}.\n"
                      "💰Balance: {:.8f}.")
telegram_error_post = "‼️An error occurred: {}."
telegram_invalid_signal_post = ("😒A false signal was received:\n"
                                "\n"
                                "{}")
telegram_not_enough_balance_post = ("😶An attempt was made to buy {} ...\n"
                                    "\n"
                                    "💰Balance is too small: {:.8f}.")
telegram_buy_high_risk_post = ("🤔An attempt was made to buy {} ...\n"
                               "\n"
                               "⁉️Too risky!\n"
                               "💰Price: {:.8f}.\n"
                               "📈To sell at: {:.8f}.\n"
                               "⏳Stop-loss: {:.8f}.")
telegram_buy_price_jumps_post = ("📊An attempt was made to buy {} ...\n"
                                 "\n"
                                 "💵The price is too unstable!")
telegram_sell_price_jumps_post = ("📊An attempt was made to sell {} ...\n"
                                  "\n"
                                  "💵The price is too unstable!")
telegram_info_post = ("🏰Total coins in portfolio: {}.\n"
                      "💰Balance: {:.8f}.")
telegram_restart_post = "🔌Veles restarted."
telegram_greeting_post = "Hi, I'm Veles! Let's start ;)"
telegram_no_answer_post = "No response to the application 😓"

# telegram channel texts keys
telegram_buy_post_keys = ("pair", "amount", "price", "total", "sell", "stop")
telegram_sell_stop_post_keys = ("pair", "amount", "price", "total", "outcome", "balance_total")
telegram_error_post_keys = ("text",)
telegram_invalid_signal_post_keys = ("text", )
telegram_not_enough_balance_post_keys = ("pair", "balance")
telegram_buy_high_risk_post_keys = ("pair", "price", "sell", "stop")
telegram_buy_price_jumps_post_keys = ("pair", )
telegram_sell_price_jumps_post_keys = ("pair",)
telegram_info_post_keys = ("number", "balance_total")
