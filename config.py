import os
import logging
import strings
import platform
import credentials
import numpy as np
import pandas as pd
import multiprocessing
from datetime import datetime
from modules.logering import setup_logger

# pandas settings
pd.set_option("precision", 8)
pd.set_option("max_rows", 1000)
pd.options.display.float_format = "{:.8f}".format
pd.options.mode.chained_assignment = None

# number of cores
cores = multiprocessing.cpu_count()

# operating system
system = platform.system()

# main folders
root_path = os.path.dirname(os.path.realpath(__file__))
data_path = os.path.join(root_path, "data")
images_path = os.path.join(root_path, "images")

# other folders
ohlcv_path = os.path.join(data_path, "ohlcv")
telegram_path = os.path.join(data_path, "telegram")

# seed
seed = 42
np.random.seed(seed)

# time
time_eps = 0.01
time_init = datetime(year=1970, month=1, day=1)

# logging
log_level = logging.INFO
log_name = os.path.join(root_path, "veles.log")
logger = setup_logger("logger", log_name, log_level)

# shared space
redis_params = {"host": "localhost", "port": 6379, "db": 0}

# telegram account issues
telegram_api_id = credentials.telegram_api_id
telegram_api_hash = credentials.telegram_api_hash
telegram_phone = credentials.telegram_phone
telegram_delay = 120
telegram_source_channel = "https://t.me/top_crypto"
telegram_params = {"telegram_api_id": telegram_api_id,
                   "telegram_api_hash": telegram_api_hash,
                   "telegram_phone": telegram_phone}

# telegram bot issues
telegram_token = credentials.telegram_token
telegram_info_channel_id = credentials.telegram_info_channel_id
telegram_logs_channel_id = credentials.telegram_logs_channel_id
telegram_allowed_users = [29510251]

# telegram bot messages
telegram_posts = {"buy": strings.telegram_buy_post,
                  "sell": strings.telegram_sell_post,
                  "stop": strings.telegram_stop_post,
                  "error": strings.telegram_error_post,
                  "invalid_signal": strings.telegram_invalid_signal_post,
                  "not_enough_balance": strings.telegram_not_enough_balance_post,
                  "buy_high_risk": strings.telegram_buy_high_risk_post,
                  "buy_price_jumps": strings.telegram_buy_price_jumps_post,
                  "sell_price_jumps": strings.telegram_sell_price_jumps_post,
                  "info": strings.telegram_info_post}
telegram_posts_keys = {"buy": strings.telegram_buy_post_keys,
                       "sell": strings.telegram_sell_stop_post_keys,
                       "stop": strings.telegram_sell_stop_post_keys,
                       "error": strings.telegram_error_post_keys,
                       "invalid_signal": strings.telegram_invalid_signal_post_keys,
                       "not_enough_balance": strings.telegram_not_enough_balance_post_keys,
                       "buy_high_risk": strings.telegram_buy_high_risk_post_keys,
                       "buy_price_jumps": strings.telegram_buy_price_jumps_post_keys,
                       "sell_price_jumps": strings.telegram_sell_price_jumps_post_keys,
                       "info": strings.telegram_info_post_keys}

# signal
signal_test = "#cryptovipsignall [\U000100008] (+738%) \nWin/Loses/Open: 67/42/4\n" \
              "WinRate: 61% Average signal ~6hours 2min\n\n\U00010000 #DOGE \U00010000\n" \
              "Sell 0.00000100 21.95% \nBuy  0.00000082\nNow  0.00000083 1.22% (@ Bittrex)\n" \
              "Stop 0.00000080 3.00%\n\n\U00010000 Original signal quote:\n" \
              "\U00010000 #DOGE/BTC : BUY 82 #bittrex\n\n\U00010000 TARGET 1 : 95\n\n" \
              "\U00010000 TARGET 2 : 110\n\n\U00010000 TARGET 3 : 130\n\U00010000️Stop-Loss is generated (-3%)."
signal_keys = ["source", "rating", "rank", "wins", "loses", "open", "winrate",
               "coin", "sell", "sellpc", "buy", "now", "nowpc", "stop", "stoppc"]

# bittrex
price_columns = ["Open", "High", "Low", "Close"]
bittrex_key = credentials.bittrex_key
bittrex_secret = credentials.bittrex_secret
bittrex_balance_splits = 13
